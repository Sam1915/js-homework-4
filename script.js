let numFirst = +prompt('Select the first number');
let numSecond = +prompt('Select the second number');
let operator = prompt('Select the operator');

function calcResult(numFirst, numSecond, operator) {
    switch (operator) {
        case '+':
            return numFirst + numSecond;
        case '-':
            return numFirst - numSecond;
        case '*':
            return numFirst * numSecond;
        case '/':
            return numFirst / numSecond;
    }
}
console.log(calcResult(numFirst, numSecond, operator));